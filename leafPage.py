import interPage
import bPTree

class leafPage:
    #n e o tamanho da pagina
    n = 0
    #kValues e o par chave valor
    kValues = []
    #dadPont aponta para a pagina pai
    dadPont = None
    #tree e referencia a arvore
    tree = None
    #bPont e referencia a arvore folha anterior
    bPont = None
    #fPont e referencia a arvore folha seguinte
    fPont = None

    def __init__(self, tree, num):
        self.n = num
        self.tree = tree
        self.kValues = []

    def setDad(self, page):
        self.dadPont = page

    def setBPont(self, page):
        self.bPont = page

    def setFPont(self, page):
        self.fPont = page

    def cmInsert(self, x):
        (self.kValues).append(x)
        (self.kValues).sort()

    def getList(self):
        return self.kValues

    def getDad(self):
        return self.dadPont

    def insert(self, x):

        #insert comum
        self.cmInsert(x)

        #se precisar fazer split
        if len(self.kValues) == self.n:

            #se a pagina for a raiz
            if self is (self.tree).getRoot():

                #cria 2 paginas leaf e uma inter
                p1 = interPage.interPage(self.tree, self.n)
                p2 = leafPage(self.tree, self.n)
                p3 = leafPage(self.tree, self.n)

                p2.setBPont(p3)
                p3.setFPont(p2)
                p2.setDad(p1)
                p3.setDad(p1)

                #pega o meio dos valores
                half = int(self.n/2)

                #salva os kValues nas duas paginas folha
                for i in range(0, half):
                    p2.cmInsert(self.kValues[i])
                for i in range(half, self.n):
                    p3.cmInsert(self.kValues[i])
                
                #insere um par kPont em p1 que aponte para p2
                p1.cmInsert([(self.kValues[half])[0], p2])

                #pega o ultimo ponteiro de p1 e insere p3
                p1.setNewPont(None, p3)

                (self.tree).setRoot(p1)

                #nao retorna nada
                #print "p1: " + str(p1.getList())
                #print "p2: " + str(p2.getList())
                #print "p3: " + str(p3.getList())
                return [False, None]

            #caso a pagina nao seja a raiz
            else:

                #cria duas paginas folha
                p1 = leafPage(self.tree, self.n)
                p2 = leafPage(self.tree, self.n)

                p1.setBPont = self.bPont
                p1.setFPont = p2
                p2.setBPont = p1

                p1.setDad(self.dadPont)
                p2.setDad(self.dadPont)

                half = int(self.n/2)

                #salva os kValues nas duas paginas folha
                for i in range(0, half):
                    p1.cmInsert(self.kValues[i])
                for i in range(half, self.n):
                    p2.cmInsert(self.kValues[i])

                #coloca p2 no ponteiro que apontava para esta pagina
                (self.dadPont).setNewPont(self, p2)

                #retorna um par kPont que aponta para p1
                group = [True, [(self.kValues[half])[0], p1]]
                #print "p1: " + str(p1.getList())
                #print "p2: " + str(p2.getList())
                #print "Oq sobe: " +str(group)
                return group

        #caso nenhum dos dois, retorna None
        return [False, None]

    def search(self, x):

        #passa por todos as chaves da pagina buscando
        for i in range(0, len(self.kValues)):

            #se encontra, retorna o valor correspondente aquela chave
            if (self.kValues[i])[0] == x:
                #print "Achei"
                return (self.kValues[i])[1]

        #caso nao encontre, retorna a seguinte string
        return "A chave nao foi encontrada."
