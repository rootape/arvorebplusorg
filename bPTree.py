import interPage
import leafPage

class bPTree:
    #n sera o tamanho das paginas da arvore
    n = 0
    #root sera a raiz da arvore
    root = None

    def __init__(self, num):
        self.n = num
        self.setRoot(leafPage.leafPage(self, self.n))

    def insert(self, key, value):
        group = [key, value]
        (self.root).insert(group)

    def getRoot(self):
        return self.root

    def setRoot(self, page):
        self.root = page

    def search(self, x):
        return (self.root).search(x)

    def printTree(self):
        aux = []
        aux.append(self.root.getList())
        #print "A propria: " + str(self.root)
        print self.root.getList()
        if isinstance(self.root, interPage.interPage):
            for i in self.root.getList():
                aux.append(i)

        aux.pop(0)
        
        while aux:
            print " "
            print "A propria: " + str((aux[0])[1])
            print "O pai " + str((aux[0])[1].getDad())
            print (aux[0])[1].getList()
            if isinstance((aux[0])[1], interPage.interPage):
                for i in (aux[0])[1].getList():
                   aux.append(i)
            aux.pop(0)
        

    
